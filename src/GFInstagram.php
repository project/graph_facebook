<?php


namespace Drupal\graph_facebook;


use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Site\Settings;

class GFInstagram implements GFInstagramInterface, ContainerInjectionInterface {

  public static function create(ContainerInterface $container) {
    // TODO: Implement create() method.
  }

  public function request($url) {
    $client = \Drupal::httpClient();
    $data = [];
    try {
      $response = $client->get($url, ['headers' => ['Accept' => 'application/json']]);
      $data = Json::decode($response->getBody());
    }
    catch (RequestException $e) {
      \Drupal::logger('graph_facebook')->error($e->getMessage());
    }
    return $data;
  }


  public function refreshToken(array $settings, $force = FALSE) {
    $lifetime = $settings['updated'] + $settings['token_lifetime'] * 86400;

    // Instagram refresh token
    $refresh_token = Settings::get('graph_facebook.refresh_access_token', FALSE);
    if (($refresh_token && $lifetime <= time()) || $force ==  TRUE) {
      $access_token = $settings['access_token'];
      // code to refresh token
      $query = http_build_query([
        'grant_type' => 'ig_refresh_token',
        'access_token' => $access_token
      ]);
      $refresh_token_url = "https://graph.instagram.com/refresh_access_token?" . $query;
      $refresh_token_result = $this->request($refresh_token_url);
      $settings['access_token'] = $refresh_token_result['access_token'];
      $settings['expires_in'] = $refresh_token_result['expires_in'];
      $settings['updated'] = time();
      \Drupal::state()->set('graph_facebook.settings', serialize($settings));
    }
    return $settings;
  }
}
