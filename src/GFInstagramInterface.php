<?php
namespace Drupal\graph_facebook;

interface GFInstagramInterface {

  public function request($url);

  public function refreshToken(array $settings, $force = FALSE);
}
