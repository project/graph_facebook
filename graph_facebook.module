<?php

/**
 * @file
 * Handles instagram gallery logic.
 */

use Drupal\Core\Site\Settings;
use \Drupal\Core\File\FileSystem;

/**
 * Implements hook_theme().
 */
function graph_facebook_theme($existing, $type, $theme, $path) {
  $theme = [
    'paragraph__instagram_gallery' => [
      'template' => 'paragraph--instagram-gallery',
      'base hook' => 'paragraph',
    ],
  ];

  return $theme;
}

/**
 * Implements template_preprocess_paragraph__PARAGRAPHTYPE().
 */
function graph_facebook_preprocess_paragraph__instagram_gallery(&$variables) {
  $paragraph = $variables['paragraph'];
  _graph_facebook_update_posts();
  $variables['posts'] = [];
  $medias_string = \Drupal::state()->get('graph_facebook.posts');
  $slides_to_show = \Drupal::state()->get('graph_facebook.slides_to_show');
  $medias = unserialize($medias_string);
  foreach ($medias as $media) {
    $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('instagram_gallery');
    $url = $style->buildUrl($media['uri']);
    // Set Variables
    $post = [];
    $post['title'] = isset($media['caption']) ? $media['caption'] : '';
    $post['image']['url'] = $url;
    $post['image']['alt'] = isset($media['caption']) ? $media['caption'] : '';
    $post['link']['uri'] = $url;
    $variables['posts'][] = $post;
  }
  $slices = (int) $paragraph->get('field_instagram_gallery_slices')->getString();
  $variables['#attached']['drupalSettings']['gfInstagram']['slickOptions'] = [
    'slidesToShow' => $slices,
    'slidesToScroll' => 1,
  ];
  $variables['#attached']['library'][] =  'graph_facebook/instagram';
}

/**
 * @param bool $force
 * Update instagram photos. By default each 1h but it can be change on administration
 */
function _graph_facebook_update_posts($force = FALSE) {
  /** @var \Drupal\Core\File\FileSystemInterface $file_system */
  $file_system = \Drupal::service('file_system');

  /** @var \Drupal\graph_facebook\GFInstagramInterface $graph_facebook_service */
  $graph_facebook_service = \Drupal::service('graph_facebook.manager');

  $settings = \Drupal::state()->get('graph_facebook.settings');
  $settings = unserialize($settings);
  $updated = \Drupal::state()->get('graph_facebook.updated');
  // lifetime in hours
  $lifetime = isset($settings['lifetime']) ? $settings['lifetime'] : 1;
  // Convert to second
  $lifetime *= 3600;
  if (!empty($updated) && ($lifetime > time() - $updated) && ($force == false)) {
    return;
  }
  $directory = 'public://instagram/';
  $file_system->prepareDirectory($directory, FileSystem::CREATE_DIRECTORY);

  $posts_string = \Drupal::state()->get('graph_facebook.posts');
  $posts = unserialize($posts_string);
  // API Instagram access
  $settings = \Drupal::state()->get('graph_facebook.settings');
  $settings = unserialize($settings);
  // Refresh token
  $settings = $graph_facebook_service->refreshToken($settings);

  $access_token = $settings['access_token'];
  // Get User ID
  $query  = http_build_query(['access_token' => $access_token, 'fields' => 'id,username']);
  $me_url = "https://graph.instagram.com/me?" . $query;
  $array_me = $graph_facebook_service->request($me_url);
  $client_id = $array_me['id'];
  $query = http_build_query(['access_token' => $access_token]);
  $api_url = 'https://graph.instagram.com/' . $client_id . '/media?' . $query;

  $array_data = $graph_facebook_service->request($api_url);

  if (empty($array_data)) {
    return;
  }
  $media_ids = $array_data['data'];
  // Clean old files
  $instagram_path = $file_system->realpath($directory);
  $list_ids = [];
  $list_names = [];
  foreach ($media_ids as $media_id) {
    $list_ids[] = $media_id['id'];
    $list_names[] = $media_id['id'] . '\.';
  }

  $match_old_files = "(" . implode('|', $list_names) . ")";
  $old_files = $file_system->scanDirectory($instagram_path, "*.*", ['nomask' => $match_old_files]);
  $old_filenames = [];

  foreach ($old_files as $old_file) {
    $old_filenames[] = $old_file->name;
    unlink($old_file->uri);
  }
  $posts_tmp = [];
  // Clean old files from database
  $posts = array_diff_key($posts,array_flip($list_ids));
  foreach ($posts as $key => $post) {
    if ((in_array($post['id'], $list_ids) && file_exists($post['uri']))) {
      $posts_tmp[$post['id']] = $post;
    }
  }
  $posts = $posts_tmp;

  $current_ids = [];
  foreach ($posts as $current_id) {
      $current_ids[] = $current_id['id'];
  }
  $filtered_ids = array_diff($list_ids, $current_ids);

  foreach ($filtered_ids as $filtered_id) {
    $query = http_build_query(
      ['fields' => 'id,caption,permalink,media_type,media_url,username,timestamp',
      'access_token' => $access_token]
    );
    $media_url = 'https://graph.instagram.com/' . $filtered_id . '?' . $query;

    $array_media = $graph_facebook_service->request($media_url);

    if (empty($array_media)) {
      continue;
    }

    if ($array_media['media_type'] === 'VIDEO') {
      continue;
    }

    $info = new SplFileInfo($array_media['media_url']);
    $filename = $info->getFilename();
    $filename = explode('?', $filename);
    $filename = $filename[0];
    $info = new SplFileInfo($filename);
    $ext = $info->getExtension();
    $file = file_save_data(file_get_contents($array_media['media_url']), $directory . $filtered_id . '.' . $ext, FILE_EXISTS_REPLACE);
    $array_media['uri'] = $file->getFileUri();
    $posts[] = $array_media;
  }


  \Drupal::state()->set('graph_facebook.posts', serialize($posts));
  \Drupal::state()->set('graph_facebook.updated', time());
  drupal_flush_all_caches();
}
