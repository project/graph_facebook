(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.GFInstagram = {
    attach: function (context, settings) {

        $('.slick.instagram .slick-slider-instagram').lightGallery({
          thumbnail: false,
          getCaptionFromTitleOrAlt: false,
          share: false,
          autoplay: false,
          autoplayControls: false,
          fullScreen: false,
          zoom: false,
          counter: false,
          toogleThumb: false,
          download: false,
          loop: false
        });

        var options = {
          speed: 800,
          autoplay: false,
          adaptiveHeight: true,
          infinite: false,
          arrows: true,
          dots: false
        };

        if (drupalSettings.gfInstagram.slickOptions) {
          $.extend(options, drupalSettings.gfInstagram.slickOptions);
        }

        $('.slick-slider-instagram', context).slick(options);

    }

  };
})(jQuery, Drupal, drupalSettings);
